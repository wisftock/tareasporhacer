## Aplicación de comandos

Instalar los paquetes de node

```
npm install
```

Ejemplo de uso para crear
```
node app crear -d "Estudiar"
```

Ejemplo de uso para listar
```
node app listar 
```

Ejemplo de uso para actualizar
```
node app actualizar -d Estudiar -c 
```

Ejemplo de uso para eliminar
```
node app borrar -d Estudiar 
```
