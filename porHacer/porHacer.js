const fs = require('fs');
let listadoPorHacer = [];

// guardar data
const guardarData = () => {
  let data = JSON.stringify(listadoPorHacer);
  fs.writeFile('data/data.json', data, (err) => {
    if (err) throw new err('No se pudo grabar', err);
  });
};

// cargar datos
const cargarData = () => {
  try {
    listadoPorHacer = require('../data/data.json');
  } catch (error) {
    listadoPorHacer = [];
  }
};

//  crear
const crear = (descripcion) => {
  cargarData();

  let porHacer = {
    descripcion,
    completado: false,
  };
  listadoPorHacer.push(porHacer);
  guardarData();
  return porHacer;
};

//  listar
const getListar = () => {
  cargarData();
  return listadoPorHacer;
};

// actualizar
const actualizar = (descripcion, completado = true) => {
  cargarData();
  let index = listadoPorHacer.findIndex((tarea) => {
    return tarea.descripcion === descripcion;
  });
  if (index >= 0) {
    listadoPorHacer[index].completado = completado;
    guardarData();
    return true;
  } else {
    return false;
  }
};

// borrar
const borrar = (descripcion) => {
  cargarData();

  let nuevoListado = listadoPorHacer.filter((tarea) => {
    return tarea.descripcion !== descripcion;
  });

  if (listadoPorHacer.length === nuevoListado.length) {
    return false;
  } else {
    listadoPorHacer = nuevoListado;
    guardarData();
    return true;
  }
};

module.exports = {
  crear,
  getListar,
  actualizar,
  borrar,
};
